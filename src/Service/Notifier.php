<?php

namespace App\Service;

use App\Model\Message;

abstract class Notifier
{
    protected string $to;

    abstract public function validateTo(): bool;

    abstract public function send(Message $message): void;
}