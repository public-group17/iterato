<?php

namespace App\Service\Senders;

use App\Entity\Customer;
use App\Model\Message;
use App\Service\Notifier;

class EmailSender extends Notifier
{
    public function __construct(Customer $customer)
    {
        $this->to = $customer->getEmail();
    }

    public function validateTo(): bool
    {
        $isEmail = filter_var($this->to, FILTER_VALIDATE_EMAIL);

        return $isEmail ? true : false;
    }

    /**
     * {@inheritDoc}
     *
     * @param Message $message
     */
    public function send(Message $message): void
    {
        if ($this->validateTo() === false) {
            throw new Exception("Invalid email address.");
        }

        print "Email: " . $this->to  . " message: " . $message->getBody();
    }
}
