<?php

namespace App\Service\Senders;

use App\Entity\Customer;
use App\Model\Message;
use App\Model\SmsMessage;
use App\Service\Notifier;

class SMSSender extends Notifier
{
    public function __construct(Customer $customer)
    {
        $this->to = $customer->getPhone();
    }

    public function validateTo(): bool
    {
        // TODO: Implement validateTo() method.
    }

    /**
     * {@inheritDoc}
     *
     * @param Message $message
     */
    public function send(Message $message): void
    {
        print "SMS: " . $this->to . " message: " . $message->getBody();
    }


}
