<?php

namespace App\Service;

use App\Entity\Customer;

class NotifierProxy
{
    protected $instance;

    public function __construct(Customer $customer)
    {
        $class = __NAMESPACE__ . '\\Senders\\' . ucfirst($customer->getNotificationType()). 'Sender';

        if(!class_exists($class)){
            throw new \Exception('Sender do not exist');
        }

        $this->instance = new $class($customer);
    }

    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->instance, $name], $arguments);
    }
}