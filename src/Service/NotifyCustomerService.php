<?php

namespace App\Service;

use App\EntityRepository\CustomerRepository;
use App\Model\Message;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class NotifyCustomerService
{

    private CustomerRepository $customerRepository;
    private ValidatorInterface $validator;

    public function __construct(CustomerRepository $customerRepository, ValidatorInterface $validator)
    {
        $this->customerRepository  = $customerRepository;
        $this->validator            = $validator;
    }

    public function notifyCustomer(string $code, object $requestData)
    {
        try {
            $customer = $this->customerRepository->find($code);

            if(!$customer){
                throw new \Exception('Customer not found');
            }

            $message = new Message();
            $message->setBody($requestData->body);
            $message->setType($customer->getNotificationType());

            $errors = $this->validator->validate($message);

            if (count($errors) > 0) {
                $errorMessage = '';
                foreach ($errors as $error) {
                    $errorMessage .= $error->getMessage().' ';
                }
                throw new \Exception(trim($errorMessage));
            }

            $notify = new NotifierProxy($customer);
            $notify->send($message);

        }catch (\Exception $exception)
        {
            throw new \Exception($exception->getMessage());
        }

    }
}