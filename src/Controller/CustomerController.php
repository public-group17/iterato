<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\Customer;
use App\EntityRepository\CustomerRepository;
use App\Model\Message;
use App\Service\EmailSender;
use App\Service\Messenger;
use App\Service\NotifyCustomerService;
use App\Service\SMSSender;
use App\Service\Validator;
use App\Service\WeatherService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 *
 */
class CustomerController extends AbstractController
{

    /**
     *
     * @Route("/customer/{code}/notifications", name="customer_notifications", methods={"POST"})
     */
    public function notifyCustomer(string $code, Request $request, NotifyCustomerService $notifyCustomerService): Response
    {
        $notifyCustomerService->notifyCustomer($code, (object)json_decode($request->getContent(), true));

        return new Response("OK");
    }
}
