<?php

namespace App\EntityRepository;

use App\Entity\Customer;
use App\Model\Message;
use Doctrine\ORM\EntityRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @ORM\Entity
 */
class CustomerRepository //extends ServiceEntityRepository
{
    /**
     * {@inheritDoc}
     */
    public function getEntityClass(): string
    {
        return Customer::class;
    }

    public function find(string $code): ?Customer
    {
        $customer = new Customer();
        switch ($code){
            case 'email':
                $customer->setCode(Message::TYPE_EMAIL);
                $customer->setEmail('email@email.com');
                $customer->setNotificationType(Message::TYPE_EMAIL);
                break;
            case 'sms':
                $customer->setCode(Message::TYPE_SMS);
                $customer->setPhone('+37011111111');
                $customer->setNotificationType(Message::TYPE_SMS);
                break;
            default:
                return null;
        }

        return $customer;
    }
}