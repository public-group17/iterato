# Code review task

## Task definition

 * Create REST api create customer notifications by email or sms depends on customer settings.
 * Customer profile data is saved in database

 Request:
     url: /api/customer/{code}/notifications
    json: { body: "notification text"  }
 

----------------------------------------------

Request examples

POST http://localhost:8080/customer/{code}/notifications
Accept: application/json

{"body":"notification text"}

for get hardcoded users use code values: sms, email

